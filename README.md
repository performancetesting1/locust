# locust

Step-1: run wiremock container with the following command

```docker run -it --rm -p 8080:8080 rodolpheche/wiremock```

For testing, put the following URL in the browser:

http://localhost:8080/__admin/

which will respond with the following:

```
{
  "mappings" : [ ],
  "meta" : {
    "total" : 0
  }
}
```

Step-2: Create a test mapping using postman/similar:

```
{
    "request": {
        "method": "GET",
        "url": "/hello/world"
    },
    "response": {
        "status": 200,
        "body": "Hello world!",
        "headers": {
            "Content-Type": "text/plain"
        }
    }
}
```

You shoulg get a response similar to the below:

```
{
    "id": "3b025ec4-4d0e-4610-b21a-3b5bdce66d33",
    "request": {
        "url": "/hello/world",
        "method": "GET"
    },
    "response": {
        "status": 200,
        "body": "Hello world!",
        "headers": {
            "Content-Type": "text/plain"
        }
    },
    "uuid": "3b025ec4-4d0e-4610-b21a-3b5bdce66d33"
}
```

To verify, check the URL: http://localhost:8080/hello/world




Post URL should be: http://localhost:8080/__admin/mappings



Step-3: Install locust

```
pip install locust
```

Step-4: Clone this repo to your local machine and run the following command in the root:

```
locust -f locust_main.py --host=http://localhost:8080/ --headless -u 10 -r 10 --run-time 0h01m
```

-u specifies the number of Users to spawn, and -r specifies the spawn rate (number of users to start per second).

Expected result:

Response time percentiles (approximated)
 Type     Name                                                              50%    66%    75%    80%    90%    95%    98%    99%  99.9% 99.99%   100% # reqs
--------|------------------------------------------------------------|---------|------|------|------|------|------|------|------|------|------|------|------|
 GET      /hello/world                                                        4      4      5      6      7      7      9     25     25     25     25     55
 GET      /mobile/payg                                                        4      4      4      5      5      6     11     14     25     25     25    121
--------|------------------------------------------------------------|---------|------|------|------|------|------|------|------|------|------|------|------|
 None     Aggregated    