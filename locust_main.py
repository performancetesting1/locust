from locust import between, TaskSet, task, HttpUser

class UserTasks(TaskSet):
    @task(1)
    def profile(self):
        self.client.get("hello/world")
    @task(2)
    def index(self):
        self.client.get("mobile/payg")

class WebsiteUser(HttpUser):
    """
    User class that does requests to the locust web server running on localhost
    """

    host = "http://127.0.0.1:8080"
    wait_time = between(2, 5)
    tasks = [UserTasks]
